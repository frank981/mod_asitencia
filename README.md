# ABOUT [![Build Status](https://travis-ci.org/danmarsden/moodle-mod_eabcattalta.svg?branch=master)](https://travis-ci.org/danmarsden/moodle-mod_eabcattalta)

The eabcattalta module is supported and maintained by Dan Marsden http://danmarsden.com

The eabcattalta module was previously developed by
    Dmitry Pupinin, Novosibirsk, Russia,
    Artem Andreev, Taganrog, Russia.

# PURPOSE
The eabcattalta module allows teachers to maintain a record of eabcattalta, replacing or supplementing a paper-based eabcattalta register.
It is primarily used in blended-learning environments where students are required to attend classes, lectures and tutorials and allows
the teacher to track and optionally provide a grade for the students eabcattalta.

Sessions can be configured to allow students to record their own eabcattalta and a range of different reports are available.

# DOCUMENTATION
https://docs.moodle.org/en/eabcattalta_activity
