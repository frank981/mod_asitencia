<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web Services for eabcattalta plugin.
 *
 * @package    mod_eabcattalta
 * @copyright  2015 Caio Bressan Doneda
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once(dirname(__FILE__).'/../locallib.php');
require_once(dirname(__FILE__).'/structure.php');
require_once(dirname(__FILE__).'/../../../lib/sessionlib.php');
require_once(dirname(__FILE__).'/../../../lib/datalib.php');

/**
 * Class eabcattalta_handler
 * @copyright  2015 Caio Bressan Doneda
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class eabcattalta_handler {
    /**
     * For this user, this method searches in all the courses that this user has permission to take eabcattalta,
     * looking for today sessions and returns the courses with the sessions.
     * @param int $userid
     * @return array
     */
    public static function get_courses_with_today_sessions($userid) {
        $usercourses = enrol_get_users_courses($userid);
        $eabcattaltainstance = get_all_instances_in_courses('eabcattalta', $usercourses);

        $coursessessions = array();

        foreach ($eabcattaltainstance as $eabcattalta) {
            $context = context_course::instance($eabcattalta->course);
            if (has_capability('mod/eabcattalta:takeeabcattaltas', $context, $userid)) {
                $course = $usercourses[$eabcattalta->course];
                if (!isset($course->eabcattalta_instance)) {
                    $course->eabcattalta_instance = array();
                }

                $att = new stdClass();
                $att->id = $eabcattalta->id;
                $att->course = $eabcattalta->course;
                $att->name = $eabcattalta->name;
                $att->grade = $eabcattalta->grade;

                $cm = new stdClass();
                $cm->id = $eabcattalta->coursemodule;

                $att = new mod_eabcattalta_structure($att, $cm, $course, $context);
                $course->eabcattalta_instance[$att->id] = array();
                $course->eabcattalta_instance[$att->id]['name'] = $att->name;
                $todaysessions = $att->get_today_sessions();

                if (!empty($todaysessions)) {
                    $course->eabcattalta_instance[$att->id]['today_sessions'] = $todaysessions;
                    $coursessessions[$course->id] = $course;
                }
            }
        }

        return self::prepare_data($coursessessions);
    }

    /**
     * Prepare data.
     *
     * @param array $coursessessions
     * @return array
     */
    private static function prepare_data($coursessessions) {
        $courses = array();

        foreach ($coursessessions as $c) {
            $courses[$c->id] = new stdClass();
            $courses[$c->id]->shortname = $c->shortname;
            $courses[$c->id]->fullname = $c->fullname;
            $courses[$c->id]->eabcattalta_instances = $c->eabcattalta_instance;
        }

        return $courses;
    }

    /**
     * For this session, returns all the necessary data to take an eabcattalta.
     *
     * @param int $sessionid
     * @return mixed
     */
    public static function get_session($sessionid) {
        global $DB;

        $session = $DB->get_record('eabcattalta_sessions', array('id' => $sessionid));
        $session->courseid = $DB->get_field('eabcattalta', 'course', array('id' => $session->eabcattaltaid));
        $session->statuses = eabcattalta_get_statuses($session->eabcattaltaid, true, $session->statusset);
        $coursecontext = context_course::instance($session->courseid);
        $session->users = get_enrolled_users($coursecontext, 'mod/eabcattalta:canbelisted',
                                             $session->groupid, 'u.id, u.firstname, u.lastname');
        $session->eabcattalta_log = array();

        if ($eabcattaltalog = $DB->get_records('eabcattalta_log', array('sessionid' => $sessionid),
                                              '', 'studentid, statusid, remarks, id')) {
            $session->eabcattalta_log = $eabcattaltalog;
        }

        return $session;
    }

    /**
     * Update user status
     *
     * @param int $sessionid
     * @param int $studentid
     * @param int $takenbyid
     * @param int $statusid
     * @param int $statusset
     */
    public static function update_user_status($sessionid, $studentid, $takenbyid, $statusid, $statusset) {
        global $DB;

        $record = new stdClass();
        $record->statusset = $statusset;
        $record->sessionid = $sessionid;
        $record->timetaken = time();
        $record->takenby = $takenbyid;
        $record->statusid = $statusid;
        $record->studentid = $studentid;

        if ($eabcattaltalog = $DB->get_record('eabcattalta_log', array('sessionid' => $sessionid, 'studentid' => $studentid))) {
            $record->id = $eabcattaltalog->id;
            $DB->update_record('eabcattalta_log', $record);
        } else {
            $DB->insert_record('eabcattalta_log', $record);
        }

        if ($eabcattaltasession = $DB->get_record('eabcattalta_sessions', array('id' => $sessionid))) {
            $eabcattaltasession->lasttaken = time();
            $eabcattaltasession->lasttakenby = $takenbyid;
            $eabcattaltasession->timemodified = time();

            $DB->update_record('eabcattalta_sessions', $eabcattaltasession);
        }
    }
}
