<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eabcattalta plugin settings
 *
 * @package    mod_eabcattalta
 * @copyright  2013 Netspot, Tim Lock.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    require_once(dirname(__FILE__).'/lib.php');
    require_once(dirname(__FILE__).'/locallib.php');

    $tabmenu = eabcattalta_print_settings_tabs();

    $settings->add(new admin_setting_heading('eabcattalta_header', '', $tabmenu));

    $plugininfos = core_plugin_manager::instance()->get_plugins_of_type('local');

    // Paging options.
    $options = array(
          0 => get_string('donotusepaging', 'eabcattalta'),
         25 => 25,
         50 => 50,
         75 => 75,
         100 => 100,
         250 => 250,
         500 => 500,
         1000 => 1000,
    );

    $settings->add(new admin_setting_configselect('eabcattalta/resultsperpage',
        get_string('resultsperpage', 'eabcattalta'), get_string('resultsperpage_desc', 'eabcattalta'), 25, $options));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/studentscanmark',
        get_string('studentscanmark', 'eabcattalta'), get_string('studentscanmark_desc', 'eabcattalta'), 1));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/studentscanmarksessiontime',
        get_string('studentscanmarksessiontime', 'eabcattalta'),
        get_string('studentscanmarksessiontime_desc', 'eabcattalta'), 1));

    $settings->add(new admin_setting_configtext('eabcattalta/studentscanmarksessiontimeend',
        get_string('studentscanmarksessiontimeend', 'eabcattalta'),
        get_string('studentscanmarksessiontimeend_desc', 'eabcattalta'), '60', PARAM_INT));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/subnetactivitylevel',
        get_string('subnetactivitylevel', 'eabcattalta'),
        get_string('subnetactivitylevel_desc', 'eabcattalta'), 1));

    $options = array(
        eabcattalta_ATT_VIEW_ALL => get_string('all', 'eabcattalta'),
        eabcattalta_eabcattalta_ATT_VIEW_ALLPAST => get_string('allpast', 'eabcattalta'),
        eabcattalta_ATT_VIEW_NOTPRESENT => get_string('below', 'eabcattalta', 'X'),
        eabcattalta_ATT_VIEW_MONTHS => get_string('months', 'eabcattalta'),
        eabcattalta_ATT_VIEW_WEEKS => get_string('weeks', 'eabcattalta'),
        eabcattalta_ATT_VIEW_DAYS => get_string('days', 'eabcattalta')
    );

    $settings->add(new admin_setting_configselect('eabcattalta/defaultview',
        get_string('defaultview', 'eabcattalta'),
            get_string('defaultview_desc', 'eabcattalta'), eabcattalta_ATT_VIEW_WEEKS, $options));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/multisessionexpanded',
        get_string('multisessionexpanded', 'eabcattalta'),
        get_string('multisessionexpanded_desc', 'eabcattalta'), 0));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/showsessiondescriptiononreport',
        get_string('showsessiondescriptiononreport', 'eabcattalta'),
        get_string('showsessiondescriptiononreport_desc', 'eabcattalta'), 0));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/studentrecordingexpanded',
        get_string('studentrecordingexpanded', 'eabcattalta'),
        get_string('studentrecordingexpanded_desc', 'eabcattalta'), 1));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/enablecalendar',
        get_string('enablecalendar', 'eabcattalta'),
        get_string('enablecalendar_desc', 'eabcattalta'), 1));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/enablewarnings',
        get_string('enablewarnings', 'eabcattalta'),
        get_string('enablewarnings_desc', 'eabcattalta'), 0));

    $name = new lang_string('defaultsettings', 'mod_eabcattalta');
    $description = new lang_string('defaultsettings_help', 'mod_eabcattalta');
    $settings->add(new admin_setting_heading('defaultsettings', $name, $description));

    $settings->add(new admin_setting_configtext('eabcattalta/subnet',
        get_string('requiresubnet', 'eabcattalta'), get_string('requiresubnet_help', 'eabcattalta'), '', PARAM_RAW));

    $name = new lang_string('defaultsessionsettings', 'mod_eabcattalta');
    $description = new lang_string('defaultsessionsettings_help', 'mod_eabcattalta');
    $settings->add(new admin_setting_heading('defaultsessionsettings', $name, $description));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/calendarevent_default',
        get_string('calendarevent', 'eabcattalta'), '', 1));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/absenteereport_default',
        get_string('includeabsentee', 'eabcattalta'), '', 1));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/studentscanmark_default',
        get_string('studentscanmark', 'eabcattalta'), '', 0));

    $options = eabcattalta_get_automarkoptions();

    $settings->add(new admin_setting_configselect('eabcattalta/automark_default',
        get_string('automark', 'eabcattalta'), '', 0, $options));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/randompassword_default',
        get_string('randompassword', 'eabcattalta'), '', 0));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/includeqrcode_default',
        get_string('includeqrcode', 'eabcattalta'), '', 0));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/autoassignstatus',
        get_string('autoassignstatus', 'eabcattalta'), '', 0));

    $options = eabcattalta_get_sharedipoptions();
    $settings->add(new admin_setting_configselect('eabcattalta/preventsharedip',
        get_string('preventsharedip', 'eabcattalta'),
        '', eabcattalta_SHAREDIP_DISABLED, $options));

    $settings->add(new admin_setting_configtext('eabcattalta/preventsharediptime',
        get_string('preventsharediptime', 'eabcattalta'), get_string('preventsharediptime_help', 'eabcattalta'), '', PARAM_RAW));

    $name = new lang_string('defaultwarningsettings', 'mod_eabcattalta');
    $description = new lang_string('defaultwarningsettings_help', 'mod_eabcattalta');
    $settings->add(new admin_setting_heading('defaultwarningsettings', $name, $description));

    $options = array();
    for ($i = 1; $i <= 100; $i++) {
        $options[$i] = "$i%";
    }
    $settings->add(new admin_setting_configselect('eabcattalta/warningpercent',
        get_string('warningpercent', 'eabcattalta'), get_string('warningpercent_help', 'eabcattalta'), 70, $options));

    $options = array();
    for ($i = 1; $i <= 50; $i++) {
        $options[$i] = "$i";
    }
    $settings->add(new admin_setting_configselect('eabcattalta/warnafter',
        get_string('warnafter', 'eabcattalta'), get_string('warnafter_help', 'eabcattalta'), 5, $options));

    $settings->add(new admin_setting_configselect('eabcattalta/maxwarn',
        get_string('maxwarn', 'eabcattalta'), get_string('maxwarn_help', 'eabcattalta'), 1, $options));

    $settings->add(new admin_setting_configcheckbox('eabcattalta/emailuser',
        get_string('emailuser', 'eabcattalta'), get_string('emailuser_help', 'eabcattalta'), 1));

    $settings->add(new admin_setting_configtext('eabcattalta/emailsubject',
        get_string('emailsubject', 'eabcattalta'), get_string('emailsubject_help', 'eabcattalta'),
        get_string('emailsubject_default', 'eabcattalta'), PARAM_RAW));


    $settings->add(new admin_setting_configtextarea('eabcattalta/emailcontent',
        get_string('emailcontent', 'eabcattalta'), get_string('emailcontent_help', 'eabcattalta'),
        get_string('emailcontent_default', 'eabcattalta'), PARAM_RAW));

}
