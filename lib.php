<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of functions and constants for module eabcattalta
 *
 * @package   mod_eabcattalta
 * @copyright  2011 Artem Andreev <andreev.artem@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once(dirname(__FILE__) . '/classes/calendar_helpers.php');

/**
 * Returns the information if the module supports a feature
 *
 * @see plugin_supports() in lib/moodlelib.php
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function eabcattalta_supports($feature) {
    switch($feature) {
        case FEATURE_GRADE_HAS_GRADE:
            return true;
        case FEATURE_GROUPS:
            return true;
        case FEATURE_GROUPINGS:
            return true;
        case FEATURE_SHOW_DESCRIPTION:
            return true;
        case FEATURE_MOD_INTRO:
            return true;
        case FEATURE_BACKUP_MOODLE2:
            return true;
        // Artem Andreev: AFAIK it's not tested.
        case FEATURE_COMPLETION_TRACKS_VIEWS:
            return false;
        default:
            return null;
    }
}

/**
 * Add default set of statuses to the new eabcattalta.
 *
 * @param int $attid - id of eabcattalta instance.
 */
function eabcattalta_att_add_default_statuses($attid) {
    global $DB;

    $statuses = $DB->get_recordset('eabcattalta_statuses', array('eabcattaltaid' => 0), 'id');
    foreach ($statuses as $st) {
        $rec = $st;
        $rec->eabcattaltaid = $attid;
        $DB->insert_record('eabcattalta_statuses', $rec);
    }
    $statuses->close();
}

/**
 * Add default set of warnings to the new eabcattalta.
 *
 * @param int $id - id of eabcattalta instance.
 */
function eabcattalta_add_default_warnings($id) {
    global $DB, $CFG;
    require_once($CFG->dirroot.'/mod/eabcattalta/locallib.php');

    $warnings = $DB->get_recordset('eabcattalta_warning',
        array('idnumber' => 0), 'id');
    foreach ($warnings as $n) {
        $rec = $n;
        $rec->idnumber = $id;
        $DB->insert_record('eabcattalta_warning', $rec);
    }
    $warnings->close();
}

/**
 * Add new eabcattalta instance.
 *
 * @param stdClass $eabcattalta
 * @return bool|int
 */
function eabcattalta_add_instance($eabcattalta) {
    global $DB;

    $eabcattalta->timemodified = time();

    $eabcattalta->id = $DB->insert_record('eabcattalta', $eabcattalta);

    eabcattalta_att_add_default_statuses($eabcattalta->id);

    eabcattalta_add_default_warnings($eabcattalta->id);

    eabcattalta_grade_item_update($eabcattalta);

    return $eabcattalta->id;
}

/**
 * Update existing eabcattalta instance.
 *
 * @param stdClass $eabcattalta
 * @return bool
 */
function eabcattalta_update_instance($eabcattalta) {
    global $DB;

    $eabcattalta->timemodified = time();
    $eabcattalta->id = $eabcattalta->instance;

    if (! $DB->update_record('eabcattalta', $eabcattalta)) {
        return false;
    }

    eabcattalta_grade_item_update($eabcattalta);

    return true;
}

/**
 * Delete existing eabcattalta
 *
 * @param int $id
 * @return bool
 */
function eabcattalta_delete_instance($id) {
    global $DB, $CFG;
    require_once($CFG->dirroot.'/mod/eabcattalta/locallib.php');

    if (! $eabcattalta = $DB->get_record('eabcattalta', array('id' => $id))) {
        return false;
    }

    if ($sessids = array_keys($DB->get_records('eabcattalta_sessions', array('eabcattaltaid' => $id), '', 'id'))) {
        if (eabcattalta_existing_calendar_events_ids($sessids)) {
            eabcattalta_delete_calendar_events($sessids);
        }
        $DB->delete_records_list('eabcattalta_log', 'sessionid', $sessids);
        $DB->delete_records('eabcattalta_sessions', array('eabcattaltaid' => $id));
    }
    $DB->delete_records('eabcattalta_statuses', array('eabcattaltaid' => $id));

    $DB->delete_records('eabcattalta_warning', array('idnumber' => $id));

    $DB->delete_records('eabcattalta', array('id' => $id));

    eabcattalta_grade_item_delete($eabcattalta);

    return true;
}

/**
 * Called by course/reset.php
 * @param moodleform $mform form passed by reference
 */
function eabcattalta_reset_course_form_definition(&$mform) {
    $mform->addElement('header', 'eabcattaltaheader', get_string('modulename', 'eabcattalta'));

    $mform->addElement('static', 'description', get_string('description', 'eabcattalta'),
                                get_string('resetdescription', 'eabcattalta'));
    $mform->addElement('checkbox', 'reset_eabcattalta_log', get_string('deletelogs', 'eabcattalta'));

    $mform->addElement('checkbox', 'reset_eabcattalta_sessions', get_string('deletesessions', 'eabcattalta'));
    $mform->disabledIf('reset_eabcattalta_sessions', 'reset_eabcattalta_log', 'notchecked');

    $mform->addElement('checkbox', 'reset_eabcattalta_statuses', get_string('resetstatuses', 'eabcattalta'));
    $mform->setAdvanced('reset_eabcattalta_statuses');
    $mform->disabledIf('reset_eabcattalta_statuses', 'reset_eabcattalta_log', 'notchecked');
}

/**
 * Course reset form defaults.
 *
 * @param stdClass $course
 * @return array
 */
function eabcattalta_reset_course_form_defaults($course) {
    return array('reset_eabcattalta_log' => 0, 'reset_eabcattalta_statuses' => 0, 'reset_eabcattalta_sessions' => 0);
}

/**
 * Reset user data within eabcattalta.
 *
 * @param stdClass $data
 * @return array
 */
function eabcattalta_reset_userdata($data) {
    global $DB;

    $status = array();

    $attids = array_keys($DB->get_records('eabcattalta', array('course' => $data->courseid), '', 'id'));

    if (!empty($data->reset_eabcattalta_log)) {
        $sess = $DB->get_records_list('eabcattalta_sessions', 'eabcattaltaid', $attids, '', 'id');
        if (!empty($sess)) {
            list($sql, $params) = $DB->get_in_or_equal(array_keys($sess));
            $DB->delete_records_select('eabcattalta_log', "sessionid $sql", $params);
            list($sql, $params) = $DB->get_in_or_equal($attids);
            $DB->set_field_select('eabcattalta_sessions', 'lasttaken', 0, "eabcattaltaid $sql", $params);
            if (empty($data->reset_eabcattalta_sessions)) {
                // If sessions are being retained, clear automarkcompleted value.
                $DB->set_field_select('eabcattalta_sessions', 'automarkcompleted', 0, "eabcattaltaid $sql", $params);
            }

            $status[] = array(
                'component' => get_string('modulenameplural', 'eabcattalta'),
                'item' => get_string('eabcattaltadata', 'eabcattalta'),
                'error' => false
            );
        }
    }

    if (!empty($data->reset_eabcattalta_statuses)) {
        $DB->delete_records_list('eabcattalta_statuses', 'eabcattaltaid', $attids);
        foreach ($attids as $attid) {
            eabcattalta_att_add_default_statuses($attid);
        }

        $status[] = array(
            'component' => get_string('modulenameplural', 'eabcattalta'),
            'item' => get_string('sessions', 'eabcattalta'),
            'error' => false
        );
    }

    if (!empty($data->reset_eabcattalta_sessions)) {
        $sessionsids = array_keys($DB->get_records_list('eabcattalta_sessions', 'eabcattaltaid', $attids, '', 'id'));
        if (eabcattalta_existing_calendar_events_ids($sessionsids)) {
            eabcattalta_delete_calendar_events($sessionsids);
        }
        $DB->delete_records_list('eabcattalta_sessions', 'eabcattaltaid', $attids);

        $status[] = array(
            'component' => get_string('modulenameplural', 'eabcattalta'),
            'item' => get_string('statuses', 'eabcattalta'),
            'error' => false
        );
    }

    return $status;
}
/**
 * Return a small object with summary information about what a
 *  user has done with a given particular instance of this module
 *  Used for user activity reports.
 *  $return->time = the time they did it
 *  $return->info = a short text description
 *
 * @param stdClass $course - full course record.
 * @param stdClass $user - full user record
 * @param stdClass $mod
 * @param stdClass $eabcattalta
 * @return stdClass.
 */
function eabcattalta_user_outline($course, $user, $mod, $eabcattalta) {
    global $CFG;
    require_once(dirname(__FILE__).'/locallib.php');
    require_once($CFG->libdir.'/gradelib.php');

    $grades = grade_get_grades($course->id, 'mod', 'eabcattalta', $eabcattalta->id, $user->id);

    $result = new stdClass();
    if (!empty($grades->items[0]->grades)) {
        $grade = reset($grades->items[0]->grades);
        $result->time = $grade->dategraded;
    } else {
        $result->time = 0;
    }
    if (has_capability('mod/eabcattalta:canbelisted', $mod->context, $user->id)) {
        $summary = new mod_eabcattalta_summary($eabcattalta->id, $user->id);
        $usersummary = $summary->get_all_sessions_summary_for($user->id);

        $result->info = $usersummary->pointsallsessions;
    }

    return $result;
}
/**
 * Print a detailed representation of what a  user has done with
 * a given particular instance of this module, for user activity reports.
 *
 * @param stdClass $course
 * @param stdClass $user
 * @param stdClass $mod
 * @param stdClass $eabcattalta
 */
function eabcattalta_user_complete($course, $user, $mod, $eabcattalta) {
    global $CFG;

    require_once(dirname(__FILE__).'/renderhelpers.php');
    require_once($CFG->libdir.'/gradelib.php');

    if (has_capability('mod/eabcattalta:canbelisted', $mod->context, $user->id)) {
        echo eabcattalta_construct_full_user_stat_html_table($eabcattalta, $user);
    }
}

/**
 * Dummy function - must exist to allow quick editing of module name.
 *
 * @param stdClass $eabcattalta
 * @param int $userid
 * @param bool $nullifnone
 */
function eabcattalta_update_grades($eabcattalta, $userid=0, $nullifnone=true) {
    // We need this function to exist so that quick editing of module name is passed to gradebook.
}
/**
 * Create grade item for given eabcattalta
 *
 * @param stdClass $eabcattalta object with extra cmidnumber
 * @param mixed $grades optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return int 0 if ok, error code otherwise
 */
function eabcattalta_grade_item_update($eabcattalta, $grades=null) {
    global $CFG, $DB;

    require_once('locallib.php');

    if (!function_exists('grade_update')) { // Workaround for buggy PHP versions.
        require_once($CFG->libdir.'/gradelib.php');
    }

    if (!isset($eabcattalta->courseid)) {
        $eabcattalta->courseid = $eabcattalta->course;
    }
    if (!$DB->get_record('course', array('id' => $eabcattalta->course))) {
        error("Course is misconfigured");
    }

    if (!empty($eabcattalta->cmidnumber)) {
        $params = array('itemname' => $eabcattalta->name, 'idnumber' => $eabcattalta->cmidnumber);
    } else {
        // MDL-14303.
        $params = array('itemname' => $eabcattalta->name);
    }

    if ($eabcattalta->grade > 0) {
        $params['gradetype'] = GRADE_TYPE_VALUE;
        $params['grademax']  = $eabcattalta->grade;
        $params['grademin']  = 0;
    } else if ($eabcattalta->grade < 0) {
        $params['gradetype'] = GRADE_TYPE_SCALE;
        $params['scaleid']   = -$eabcattalta->grade;

    } else {
        $params['gradetype'] = GRADE_TYPE_NONE;
    }

    if ($grades === 'reset') {
        $params['reset'] = true;
        $grades = null;
    }

    return grade_update('mod/eabcattalta', $eabcattalta->courseid, 'mod', 'eabcattalta', $eabcattalta->id, 0, $grades, $params);
}

/**
 * Delete grade item for given eabcattalta
 *
 * @param object $eabcattalta object
 * @return object eabcattalta
 */
function eabcattalta_grade_item_delete($eabcattalta) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    if (!isset($eabcattalta->courseid)) {
        $eabcattalta->courseid = $eabcattalta->course;
    }

    return grade_update('mod/eabcattalta', $eabcattalta->courseid, 'mod', 'eabcattalta',
                        $eabcattalta->id, 0, null, array('deleted' => 1));
}

/**
 * This function returns if a scale is being used by one eabcattalta
 * it it has support for grading and scales. Commented code should be
 * modified if necessary. See book, glossary or journal modules
 * as reference.
 *
 * @param int $eabcattaltaid
 * @param int $scaleid
 * @return boolean True if the scale is used by any eabcattalta
 */
function eabcattalta_scale_used ($eabcattaltaid, $scaleid) {
    return false;
}

/**
 * Checks if scale is being used by any instance of eabcattalta
 *
 * This is used to find out if scale used anywhere
 *
 * @param int $scaleid
 * @return bool true if the scale is used by any book
 */
function eabcattalta_scale_used_anywhere($scaleid) {
    return false;
}

/**
 * Serves the eabcattalta sessions descriptions files.
 *
 * @param object $course
 * @param object $cm
 * @param object $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @return bool false if file not found, does not return if found - justsend the file
 */
function eabcattalta_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload) {
    global $DB;

    if ($context->contextlevel != CONTEXT_MODULE) {
        return false;
    }

    require_login($course, false, $cm);

    if (!$DB->record_exists('eabcattalta', array('id' => $cm->instance))) {
        return false;
    }

    // Session area is served by pluginfile.php.
    $fileareas = array('session');
    if (!in_array($filearea, $fileareas)) {
        return false;
    }

    $sessid = (int)array_shift($args);
    if (!$DB->record_exists('eabcattalta_sessions', array('id' => $sessid))) {
        return false;
    }

    $fs = get_file_storage();
    $relativepath = implode('/', $args);
    $fullpath = "/$context->id/mod_eabcattalta/$filearea/$sessid/$relativepath";
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }
    send_stored_file($file, 0, 0, true);
}

/**
 * Print tabs on eabcattalta settings page.
 *
 * @param string $selected - current selected tab.
 */
function eabcattalta_print_settings_tabs($selected = 'settings') {
    global $CFG;
    // Print tabs for different settings pages.
    $tabs = array();
    $tabs[] = new tabobject('settings', $CFG->wwwroot.'/admin/settings.php?section=modsettingeabcattalta',
        get_string('settings', 'eabcattalta'), get_string('settings'), false);

    $tabs[] = new tabobject('defaultstatus', $CFG->wwwroot.'/mod/eabcattalta/defaultstatus.php',
        get_string('defaultstatus', 'eabcattalta'), get_string('defaultstatus', 'eabcattalta'), false);

    if (get_config('eabcattalta', 'enablewarnings')) {
        $tabs[] = new tabobject('defaultwarnings', $CFG->wwwroot . '/mod/eabcattalta/warnings.php',
            get_string('defaultwarnings', 'eabcattalta'), get_string('defaultwarnings', 'eabcattalta'), false);
    }

    $tabs[] = new tabobject('coursesummary', $CFG->wwwroot.'/mod/eabcattalta/coursesummary.php',
        get_string('coursesummary', 'eabcattalta'), get_string('coursesummary', 'eabcattalta'), false);

    if (get_config('eabcattalta', 'enablewarnings')) {
        $tabs[] = new tabobject('absentee', $CFG->wwwroot . '/mod/eabcattalta/absentee.php',
            get_string('absenteereport', 'eabcattalta'), get_string('absenteereport', 'eabcattalta'), false);
    }

    $tabs[] = new tabobject('resetcalendar', $CFG->wwwroot.'/mod/eabcattalta/resetcalendar.php',
        get_string('resetcalendar', 'eabcattalta'), get_string('resetcalendar', 'eabcattalta'), false);

    $tabs[] = new tabobject('importsessions', $CFG->wwwroot . '/mod/eabcattalta/import/sessions.php',
        get_string('importsessions', 'eabcattalta'), get_string('importsessions', 'eabcattalta'), false);

    ob_start();
    print_tabs(array($tabs), $selected);
    $tabmenu = ob_get_contents();
    ob_end_clean();

    return $tabmenu;
}

/**
 * Helper function to remove a user from the thirdpartyemails record of the eabcattalta_warning table.
 *
 * @param array $warnings - list of warnings to parse.
 * @param int $userid - User id of user to remove.
 */
function eabcattalta_remove_user_from_thirdpartyemails($warnings, $userid) {
    global $DB;

    // Update the third party emails list for all the relevant warnings.
    $updatedwarnings = array_map(
        function(stdClass $warning) use ($userid) : stdClass {
            $warning->thirdpartyemails = implode(',', array_diff(explode(',', $warning->thirdpartyemails), [$userid]));
            return $warning;
        },
        array_filter(
            $warnings,
            function (stdClass $warning) use ($userid) : bool {
                return in_array($userid, explode(',', $warning->thirdpartyemails));
            }
        )
    );

    // Sadly need to update each individually, no way to bulk update as all the thirdpartyemails field can be different.
    foreach ($updatedwarnings as $updatedwarning) {
        $DB->update_record('eabcattalta_warning', $updatedwarning);
    }
}
