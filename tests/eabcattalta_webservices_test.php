<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Webservices test for eabcattalta plugin.
 *
 * @package    mod_eabcattalta
 * @copyright  2015 Caio Bressan Doneda
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');
}

global $CFG;

// Include the code to test.
require_once($CFG->dirroot . '/mod/eabcattalta/classes/eabcattalta_webservices_handler.php');
require_once($CFG->dirroot . '/mod/eabcattalta/classes/structure.php');

/**
 * This class contains the test cases for the functions in eabcattalta_webservices_handler.php.
 * @copyright  2015 Caio Bressan Doneda
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class eabcattalta_webservices_tests extends advanced_testcase {
    /** @var coursecat */
    protected $category;
    /** @var stdClass */
    protected $course;
    /** @var stdClass */
    protected $eabcattalta;
    /** @var stdClass */
    protected $teacher;
    /** @var array */
    protected $students;
    /** @var array */
    protected $sessions;

    /**
     * Setup class.
     */
    public function setUp() {
        $this->category = $this->getDataGenerator()->create_category();
        $this->course = $this->getDataGenerator()->create_course(array('category' => $this->category->id));

        $this->eabcattalta = $this->create_eabcattalta();

        $this->create_and_enrol_users();

        $this->setUser($this->teacher);

        $session = new stdClass();
        $session->sessdate = time();
        $session->duration = 6000;
        $session->description = "";
        $session->descriptionformat = 1;
        $session->descriptionitemid = 0;
        $session->timemodified = time();
        $session->statusset = 0;
        $session->groupid = 0;
        $session->absenteereport = 1;
        $session->calendarevent = 0;

        // Creating two sessions.
        $this->sessions[] = $session;

        $this->eabcattalta->add_sessions($this->sessions);
    }

    /**
     * Create new eabcattalta activity.
     */
    private function create_eabcattalta() {
        global $DB;
        $att = $this->getDataGenerator()->create_module('eabcattalta', array('course' => $this->course->id));
        $cm = $DB->get_record('course_modules', array('id' => $att->cmid));
        unset($att->cmid);
        return new mod_eabcattalta_structure($att, $cm, $this->course);
    }

    /** Creating 10 students and 1 teacher. */
    protected function create_and_enrol_users() {
        $this->students = array();
        for ($i = 0; $i < 10; $i++) {
            $student = $this->getDataGenerator()->create_user();
            $this->getDataGenerator()->enrol_user($student->id, $this->course->id, 5); // Enrol as student.
            $this->students[] = $student;
        }

        $this->teacher = $this->getDataGenerator()->create_user();
        $this->getDataGenerator()->enrol_user($this->teacher->id, $this->course->id, 3); // Enrol as teacher.
    }

    public function test_get_courses_with_today_sessions() {
        $this->resetAfterTest(true);

        // Just adding the same session again to check if the method returns the right amount of instances.
        $this->eabcattalta->add_sessions($this->sessions);

        $courseswithsessions = eabcattalta_handler::get_courses_with_today_sessions($this->teacher->id);

        $this->assertTrue(is_array($courseswithsessions));
        $this->assertEquals(count($courseswithsessions), 1);
        $course = array_pop($courseswithsessions);
        $this->assertEquals($course->fullname, $this->course->fullname);
        $eabcattaltainstance = array_pop($course->eabcattalta_instances);
        $this->assertEquals(count($eabcattaltainstance['today_sessions']), 2);
    }

    public function test_get_courses_with_today_sessions_multiple_instances() {
        $this->resetAfterTest(true);

        // Make another eabcattalta.
        $second = $this->create_eabcattalta();

        // Just add the same session.
        $secondsession = clone $this->sessions[0];
        $secondsession->sessdate += 3600;

        $second->add_sessions([$secondsession]);

        $courseswithsessions = eabcattalta_handler::get_courses_with_today_sessions($this->teacher->id);
        $this->assertTrue(is_array($courseswithsessions));
        $this->assertEquals(count($courseswithsessions), 1);
        $course = array_pop($courseswithsessions);
        $this->assertEquals(count($course->eabcattalta_instances), 2);
    }

    public function test_get_session() {
        $this->resetAfterTest(true);

        $courseswithsessions = eabcattalta_handler::get_courses_with_today_sessions($this->teacher->id);

        $course = array_pop($courseswithsessions);
        $eabcattaltainstance = array_pop($course->eabcattalta_instances);
        $session = array_pop($eabcattaltainstance['today_sessions']);

        $sessioninfo = eabcattalta_handler::get_session($session->id);

        $this->assertEquals($this->eabcattalta->id, $sessioninfo->eabcattaltaid);
        $this->assertEquals($session->id, $sessioninfo->id);
        $this->assertEquals(count($sessioninfo->users), 10);
    }

    public function test_get_session_with_group() {
        $this->resetAfterTest(true);

        // Create a group in our course, and add some students to it.
        $group = new stdClass();
        $group->courseid = $this->course->id;
        $group = $this->getDataGenerator()->create_group($group);

        for ($i = 0; $i < 5; $i++) {
            $member = new stdClass;
            $member->groupid = $group->id;
            $member->userid = $this->students[$i]->id;
            $this->getDataGenerator()->create_group_member($member);
        }

        // Add a session that's identical to the first, but with a group.
        $midnight = usergetmidnight(time()); // Check if this test is running during midnight.
        $session = clone $this->sessions[0];
        $session->groupid = $group->id;
        $session->sessdate += 3600; // Make sure it appears second in the list.
        $this->eabcattalta->add_sessions([$session]);

        $courseswithsessions = eabcattalta_handler::get_courses_with_today_sessions($this->teacher->id);

        // This test is fragile when running over midnight - check that it is still the same day, if not, run this again.
        // This isn't really ideal code, but will hopefully still give a valid test.
        if (empty($courseswithsessions) && $midnight !== usergetmidnight(time())) {
            $this->eabcattalta->add_sessions([$session]);
            $courseswithsessions = eabcattalta_handler::get_courses_with_today_sessions($this->teacher->id);
        }

        $course = array_pop($courseswithsessions);
        $eabcattaltainstance = array_pop($course->eabcattalta_instances);
        $session = array_pop($eabcattaltainstance['today_sessions']);

        $sessioninfo = eabcattalta_handler::get_session($session->id);

        $this->assertEquals($session->id, $sessioninfo->id);
        $this->assertEquals($group->id, $sessioninfo->groupid);
        $this->assertEquals(count($sessioninfo->users), 5);
    }

    public function test_update_user_status() {
        $this->resetAfterTest(true);

        $courseswithsessions = eabcattalta_handler::get_courses_with_today_sessions($this->teacher->id);

        $course = array_pop($courseswithsessions);
        $eabcattaltainstance = array_pop($course->eabcattalta_instances);
        $session = array_pop($eabcattaltainstance['today_sessions']);

        $sessioninfo = eabcattalta_handler::get_session($session->id);

        $student = array_pop($sessioninfo->users);
        $status = array_pop($sessioninfo->statuses);
        $statusset = $sessioninfo->statusset;
        eabcattalta_handler::update_user_status($session->id, $student->id, $this->teacher->id, $status->id, $statusset);

        $sessioninfo = eabcattalta_handler::get_session($session->id);
        $log = $sessioninfo->Seabcattalta_log;
        $studentlog = $log[$student->id];

        $this->assertEquals($status->id, $studentlog->statusid);
    }
}
